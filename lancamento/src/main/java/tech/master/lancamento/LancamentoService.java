package tech.master.lancamento;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

@Service
public class LancamentoService {

	private LancamentoRepository lancamentoRepository;
	private CartaoClient cartaoClient;
	/*private ClienteClient clienteClient;*/
	@Autowired
	public LancamentoService(LancamentoRepository lancamentoRepository, CartaoClient cartaoClient) {
		super();
		this.lancamentoRepository = lancamentoRepository;
		this.cartaoClient = cartaoClient;
		/*this.clienteClient = clienteClient;*/
	}

	public Lancamento criar(Lancamento lancamento) {
		/* Cartao cartao = lancamento.getCartao(); */

		Optional<Cartao> optionalCartao = cartaoClient.buscar(lancamento.getNumero());

		if (!optionalCartao.isPresent()) {
			throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
		}

		if (!optionalCartao.get().isAtivo()) {
			throw new HttpClientErrorException(HttpStatus.MOVED_PERMANENTLY);
		}

		lancamento.setHorario(LocalDateTime.now());

		return lancamentoRepository.save(lancamento);
	}

	public Iterable<Lancamento> buscarTodasPorNumeroDoCartao(String numero) {
		return lancamentoRepository.findAllByNumero(numero);
	}
}
