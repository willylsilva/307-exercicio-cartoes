package tech.master.zuul.zuulgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class ZuulGetewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZuulGetewayApplication.class, args);
	}

}
