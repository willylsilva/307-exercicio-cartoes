package tech.master.cartao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface CartaoRepository extends CrudRepository<Cartao, String>{
}
